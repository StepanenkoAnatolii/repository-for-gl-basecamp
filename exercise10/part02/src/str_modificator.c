#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/moduleparam.h>
#include <linux/stat.h>
#include <linux/slab.h>
#include <linux/ctype.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Anatolii");
MODULE_DESCRIPTION("Module to modificate strings");

static char* usr_str = "Start";

static char** split_str_words(char* str)
{
    char** words = kmalloc(strlen(str) * 3, GFP_KERNEL);
    char ch;
    int count_w = 0;
    int i = 0;
    while(str[i] != '\0')
    {
        char *buffer_str = kmalloc(strlen(str) + 1, GFP_KERNEL);
        int count_c = 0;
        while(((ch = str[i]) != ' ') && (ch != '\0'))
        {
            buffer_str[count_c] = ch;
            i++;
            count_c++;
        }
        words[count_w] = kmalloc(count_c * sizeof(char) + 1, GFP_KERNEL);
        int j = 0;
        for(j = 0; buffer_str[j] != '\0'; j++)
        {
            words[count_w][j] = buffer_str[j];
        }
        kfree(buffer_str);
        if(str[i] == ' ')
        {
            i++;
        }
        count_w++;
    }
    return words;
}

static int get_str_words_quantity(char* str)
{
    int quantity = 1;
    char ch;
    int i = 0;
    while((ch = str[i])!='\0')
    {
        if(ch == ' ')
        {
            quantity++;
        }
        i++;
    }
    return quantity;
}

static char* reverse_words(char* str)
{
    int quantity = get_str_words_quantity(str);

    char** start_words = split_str_words(str);
    char* final_str = kmalloc(strlen(str) + 1, GFP_KERNEL);
    int new_str_counter = 0;

    int buffer_counter = 0;
    while(buffer_counter < quantity)
    {
        char ch;
        int i = 0;
        while((ch = start_words[buffer_counter][i]) != '\0')
            i++;
        i--;
        while(i >= 0)
        {
            ch = start_words[buffer_counter][i];
            final_str[new_str_counter] = ch;
            new_str_counter++;
            i--;
        }
        if((buffer_counter + 1) < quantity)
        {
            final_str[new_str_counter] = ' ';
        }
        new_str_counter++;

        buffer_counter++; 
    }
    
    int words_to_free_counter = 0;
    while(words_to_free_counter < quantity)
    {
        kfree(start_words[words_to_free_counter]);
        words_to_free_counter++;
    }

    kfree(start_words);

    return final_str;
}

char* upper_str(char* str)
{
    char* new_str = kmalloc(strlen(str) + 1, GFP_KERNEL);
    char ch;
    int i = 0;
    while((ch = str[i]) != '\0')
    {
        new_str[i] = toupper(ch);
        i++;
    }
    kfree(str);
    return new_str;
}

int set_usr_str(const char *val, const struct kernel_param *kp)
{
    int res = param_set_charp(val, kp);
    if(res == 0)
    {
        printk(KERN_INFO "|====================================|");
        char* str_words_rev = reverse_words(usr_str);
        printk(KERN_INFO "Reversed: %s", str_words_rev);
        str_words_rev = upper_str(str_words_rev);
        printk(KERN_INFO "Reversed uppercase: %s", str_words_rev);
        kfree(str_words_rev);
        printk(KERN_INFO "|====================================|");
        return 0;
    }
    return -1;
}

const struct kernel_param_ops usr_str_ops =
{
    .set = &set_usr_str,
    .get = &param_get_charp,
};

module_param_cb(usr_str, &usr_str_ops, &usr_str, S_IRWXU);
MODULE_PARM_DESC(usr_str, "User string that module works with");

static int __init str_mod_init(void)
{
    printk(KERN_INFO "Test module is loaded");
    return 0;
}

static void __exit str_mod_exit(void)
{
    printk(KERN_INFO "Test module is unloaded");
}

module_init(str_mod_init);
module_exit(str_mod_exit);