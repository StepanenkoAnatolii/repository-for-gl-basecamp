#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/moduleparam.h>
#include <linux/stat.h>
#include <linux/slab.h>
#include <linux/ctype.h>
//
#include <linux/fs.h>
#include <linux/cdev.h>
#include <asm/uaccess.h>
#include <linux/pci.h>
//
#include <linux/proc_fs.h>
#include <linux/sched.h>
//

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Anatolii");
MODULE_DESCRIPTION("Module to modificate strings");

static char* reverse_words(char* str);
static char* upper_str(char* str);
static char* copy_str(char* str);

static int display_mode = 0;//variable to set way to display str in procfs


/*---------------procfs-part-start---------------*/
#define MODULE_TAG      "string_display_module "
#define PROC_DIRECTORY  "str_display"
#define PROC_FILENAME   "str"
#define BUFFER_SIZE     100

static char *proc_buffer;
static size_t proc_msg_length;
static size_t proc_msg_read_pos;

static struct proc_dir_entry *proc_dir;
static struct proc_dir_entry *proc_file;

static ssize_t example_read(struct file *file_p, char __user *buffer, size_t length, loff_t *offset);
static ssize_t example_write(struct file *file_p, const char __user *buffer, size_t length, loff_t *offset);

static const struct proc_ops _proc_fops = {
    .proc_read  = example_read,
    .proc_write = example_write,
};


static int create_buffer(void)
{
    proc_buffer = kmalloc(BUFFER_SIZE, GFP_KERNEL);
    if (NULL == proc_buffer)
        return -ENOMEM;
    proc_msg_length = 0;

    return 0;
}


static void cleanup_buffer(void)
{
    if (proc_buffer) {
        kfree(proc_buffer);
        proc_buffer = NULL;
    }
    proc_msg_length = 0;
}


static int create_proc_example(void)
{
    proc_dir = proc_mkdir(PROC_DIRECTORY, NULL);
    if (NULL == proc_dir)
        return -EFAULT;

    proc_file = proc_create(PROC_FILENAME, S_IFREG | S_IRUGO | S_IWUGO, proc_dir, &_proc_fops);
    if (NULL == proc_file)
        return -EFAULT;

    return 0;
}


static void cleanup_proc_example(void)
{
    if (proc_file)
    {
        remove_proc_entry(PROC_FILENAME, proc_dir);
        proc_file = NULL;
    }
    if (proc_dir)
    {
        remove_proc_entry(PROC_DIRECTORY, NULL);
        proc_dir = NULL;
    }
}


static ssize_t example_read(struct file *file_p, char __user *buffer, size_t length, loff_t *offset)
{
    size_t left;

    if (length > (proc_msg_length - proc_msg_read_pos))
        length = (proc_msg_length - proc_msg_read_pos);

    /*-----main-logic-start-----*/
    char* new_buffer = copy_str(proc_buffer);
    if(display_mode == 0)
    {
        //don't change
    } else if(display_mode == 1)
    {
        new_buffer = reverse_words(new_buffer);
    } else if(display_mode == 2)
    {
        new_buffer = upper_str(new_buffer);
    } else
    {
        printk(KERN_WARNING "PROCFS READING: UNKNOWN ARGUMENT");
    }
    /*------main-logic-end------*/

    left = copy_to_user(buffer, &new_buffer[proc_msg_read_pos], length);

    kfree(new_buffer);

    proc_msg_read_pos += length - left;

    if (left)
        printk(KERN_ERR MODULE_TAG "failed to read %u from %u chars\n", left, length);
    else
        printk(KERN_NOTICE MODULE_TAG "read %u chars\n", length);

    return length - left;
}

static ssize_t example_write(struct file *file_p, const char __user *buffer, size_t length, loff_t *offset)
{
    size_t msg_length;
    size_t left;

    if (length > BUFFER_SIZE)
    {
        printk(KERN_WARNING MODULE_TAG "reduse message length from %u to %u chars\n", length, BUFFER_SIZE);
        msg_length = BUFFER_SIZE;
    }
    else
        msg_length = length;

    left = copy_from_user(proc_buffer, buffer, msg_length);

    proc_msg_length = msg_length - left;
    proc_msg_read_pos = 0;

    if (left)
        printk(KERN_ERR MODULE_TAG "failed to write %u from %u chars\n", left, msg_length);
    else
    {
        printk(KERN_NOTICE MODULE_TAG "written %u chars\n", msg_length);
    }

    return length;
}
/*---------------procfs-part-end-----------------*/

/*---------------sysfs-part-start----------------*/
#define LEN_MSG 10
static char buf_msg[ LEN_MSG + 1 ] = "0";

static ssize_t method_show( struct class *class, struct class_attribute *attr, char *buf ) {
   strcpy( buf, buf_msg );
   return strlen( buf );
}

static ssize_t method_store( struct class *class, struct class_attribute *attr, const char *buf, size_t count ) {
   strncpy( buf_msg, buf, count );

   if(strcmp(buf, "0\n") == 0)
   {
      printk("no translation\n");
      display_mode = 0;
   } else if(strcmp(buf, "1\n") == 0)
   {
      printk("words reverse translation\n");
      display_mode = 1;
   } else if(strcmp(buf, "2\n") == 0)
   {
      printk("uppercase translation\n");
      display_mode = 2;
   } else
   {
      printk(KERN_WARNING "SYSFS DISPLAY_MODE: NO SUCH ARGUMENT\n");
      display_mode = 0;
   }

   buf_msg[ count ] = '\0';
   return count;
}

CLASS_ATTR_RW(method);

static struct class *str_mod;
/*---------------sysfs-part-end------------------*/
/*====================================================================*/

static char* copy_str(char* str){
    char* new_str = kmalloc(strlen(str)+1, GFP_KERNEL);
    int i;
    i = 0;
    char ch;
    while((ch = str[i]) != '\0')
    {
        new_str[i] = ch;
        i++;
    }
    return new_str;
}

static char** split_str_words(char* str)
{
    char** words = kmalloc(strlen(str) * 3, GFP_KERNEL);
    char ch;
    int count_w;
    count_w = 0;
    int i;
    i = 0;
    while(str[i] != '\0')
    {
        char *buffer_str = kmalloc(strlen(str) + 1, GFP_KERNEL);
        int count_c = 0;
        while(((ch = str[i]) != ' ') && (ch != '\0'))
        {
            buffer_str[count_c] = ch;
            i++;
            count_c++;
        }
        words[count_w] = kmalloc(count_c * sizeof(char) + 1, GFP_KERNEL);
        int j;
        j = 0;
        for(j = 0; buffer_str[j] != '\0'; j++)
        {
            words[count_w][j] = buffer_str[j];
        }
        kfree(buffer_str);
        if(str[i] == ' ')
        {
            i++;
        }
        count_w++;
    }
    return words;
}

static int get_str_words_quantity(char* str)
{
    int quantity;
    quantity = 1;
    char ch;
    int i;
    i = 0;
    while((ch = str[i])!='\0')
    {
        if(ch == ' ')
        {
            quantity++;
        }
        i++;
    }
    return quantity;
}

static char* reverse_words(char* str)
{
    int quantity = get_str_words_quantity(str);

    char** start_words = split_str_words(str);
    char* final_str = kmalloc(strlen(str) + 1, GFP_KERNEL);
    int new_str_counter = 0;

    int buffer_counter = 0;
    while(buffer_counter < quantity)
    {
        char ch;
        int i;
        i = 0;
        while((ch = start_words[buffer_counter][i]) != '\0')
            i++;
        i--;
        while(i >= 0)
        {
            ch = start_words[buffer_counter][i];
            final_str[new_str_counter] = ch;
            new_str_counter++;
            i--;
        }
        if((buffer_counter + 1) < quantity)
        {
            final_str[new_str_counter] = ' ';
        }
        new_str_counter++;

        buffer_counter++; 
    }
    
    int words_to_free_counter = 0;
    while(words_to_free_counter < quantity)
    {
        kfree(start_words[words_to_free_counter]);
        words_to_free_counter++;
    }

    kfree(start_words);

    return final_str;
}

static char* upper_str(char* str)
{
    char* new_str = kmalloc(strlen(str) + 1, GFP_KERNEL);
    char ch;
    int i;
    i = 0;
    while((ch = str[i]) != '\0')
    {
        new_str[i] = toupper(ch);
        i++;
    }
    kfree(str);
    return new_str;
}

/*====================================================================*/

static int __init str_mod_init(void)
{
    printk(KERN_INFO "Str_modif module is loaded");
    /*---------------procfs-part-start---------------*/
    int err;

    err = create_buffer();
    if (err)
        goto error;

    err = create_proc_example();
    if (err)
        goto error;

    /*---------------procfs-part-end-----------------*/
    /*---------------sysfs-part-start----------------*/
    int res;
    str_mod = class_create( THIS_MODULE, "str-mod" );
    if( IS_ERR( str_mod ) ) printk( "bad class create\n" );
    res = class_create_file( str_mod, &class_attr_method );
    /*---------------sysfs-part-end------------------*/
    return res;

    error:
    printk(KERN_ERR MODULE_TAG "failed to load\n");
    cleanup_proc_example();
    cleanup_buffer();
    return err;
}

static void __exit str_mod_exit(void)
{
    printk(KERN_INFO "str_modif module is unloaded");
    /*---------------procfs-part-start---------------*/
    cleanup_proc_example();
    cleanup_buffer();
    printk(KERN_NOTICE MODULE_TAG "exited\n");
    /*---------------procfs-part-end-----------------*/
    /*---------------sysfs-part-start----------------*/
    class_remove_file( str_mod, &class_attr_method );
    class_destroy( str_mod );
    /*---------------sysfs-part-end------------------*/
}

module_init(str_mod_init);
module_exit(str_mod_exit);