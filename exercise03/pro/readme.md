git config --global user.name "Anatolii Stepanenko"
git config --global user.email "stepanatoliink@gmail.com"
git config --global core.autocrlf input
git config --global core.safecrlf true

mkdir exercise03
cd exercise03
git init

mkdir pro
cd pro
touch readme.md
git add .
git commit -m "repo: initial commit"

git checkout -b first_branch
nano readme.md
git status
git commit -am "readme: add the command lo of the 1st subtask"

git checkout master
nano readme.md
git commit -am "readme: add command log to solve 2nd subtask"
git log --oneline --decorate --graph --all

git status
git merge first_branch
nano readme.md
git status
git log --oneline --decorate --graph --all

nano readme.md
git commit -am "readme: add command log to solve 3rd subtask"
nano readme.md
git commit -am "readme: add command log to solve 4th subtask"
nano readme.md
git commit -am "readme: add command log to solve 5th subtask"
nano readme.md
git commit -am "readme: add command log to solve 6th subtask"
nano readme.md
git commit -am "readme: add command log to solve 7th subtask"
git log --oneline --decorate --graph --all
