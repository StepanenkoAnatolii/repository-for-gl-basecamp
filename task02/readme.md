Task: write a "Guess the Number" game using the Bash script.
Subtask01(task): create a random number generator script.
Subtask01: created file for bash script
Subtask01: made first version of random number generator
Subtask01 completed.
Subtask02(task): random number generator function refactoring.
Subtask02: made function that returns random number depending on given upper bound.
Subtask02 completed.
Subtask03(task): add code to guess_the_number that compares first parametr of script with random number.
Subtask03: deleted useless comments from guess_the_number file.
Subtask03: made function that compares two numbers and used it to compare user number with random.
Subtask03 completed.
Subtask04(task): if user didn't input number as a parametr ask him to input it.
Subtask04: made request to user if he doesn't input the number as a parametr.
Subtask04 completed.
Subtask05(task): implement the ability to set the upper limit of random numbers range(from 0 to 100), by second script parametr.
Subtask05: add posibility to set upper limit of random numbers set by second parametr
Subtask05 completed.
Subtask06(task): if user didn't input upper limit as second parametr ask him to input it.
Subtask06: add asking user to input upper limit.
Subtask06 completed.
Subtask07(task): implement the ability to set the number of attempts to guess the number.
Subtask07: implemented the ability to set the number of attempts to guess.
Subtask07: implemented stopping the game when user guessed the number.
Subtask07 completed.
Subtask08(task): if user didn't input the number of attempts as third parametr, ask him to input it.
Subtask08: implemented asking user to input attempts number.
Subtask08 completed.
Subtask09(task): refactor code of guess_the_number script, add comments to it.
Subtask09: refactored the code, wrote the comments.
Subtask09 completed.
Subtask10(task): implement posibility of replay with the same parametrs while user guesses.
Subtask10: made play_game function that is the main game function and inserted the code into it.
Subtask10: implemented capability to replay with same parametrs if user wins.
Subtask10 completed.
