#include "lib/lcd3_2.h"
#include "lib/ili9341.h"
#include "lib/fonts.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define GPIO_PIN_RESET	LCD_PIN_RESET
#define GPIO_PIN_DC		LCD_PIN_DC
#define GPIO_PIN_CS		LCD_PIN_CS

static const char* device = "/dev/spidev0.0";
static uint8_t mode = 0;
static uint8_t bits = 8;
static uint32_t speed = 5000000;
static uint16_t delay = 0;

int fd;

/*------------------------------main-code---------------------------------*/
char* get_ip(){
	char *host_ip;
	char host[256];
	struct hostent *host_entry;
	int hostname = gethostname(host, sizeof(host));
	host_entry = gethostbyname(host);
	host_ip = inet_ntoa(*((struct in_addr*) host_entry->h_addr_list[0]));
	return host_ip;
}

void get_time(char *time_str){
	time_t s_time = time(NULL);
	struct tm *m_time = localtime(&s_time);
	strftime(time_str, 12, "%H:%M:%S", m_time);
}

int get_sec(){
	char time_str[4];
	time_t s_time = time(NULL);
	struct tm *m_time = localtime(&s_time);
	strftime(time_str, 4, "%S", m_time);
	return atoi(time_str);
}

void info_out(){
	char *host_ip = get_ip();
	lcd_put_string(10, (LCD_HEIGHT-20), host_ip, Font_11x18, COLOR_WHITE, COLOR_BLACK);

	uint16_t date_pos = LCD_WIDTH - 11*9;
	char *time_str = malloc(sizeof(char) * 6 + 1);

	get_time(time_str);
	lcd_put_string(date_pos, 10, time_str, Font_11x18, COLOR_WHITE, COLOR_BLACK);
	lcd_update_screen();

	free(time_str);
}

void lcd_put_number(int num){
	char num_char[5];
	char *whitespace = "        ";
	sprintf(num_char, "%d", num);
	lcd_put_string(10, (LCD_HEIGHT/2), whitespace, Font_11x18, COLOR_WHITE, COLOR_BLACK);
	lcd_put_string(10, (LCD_HEIGHT/2), num_char, Font_11x18, COLOR_WHITE, COLOR_BLACK);
}

void button_calc(){
	FILE *fexport18 = fopen("/sys/class/gpio/export", "w");
	fprintf(fexport18, "18");
	fclose(fexport18);
	FILE *fexport23 = fopen("/sys/class/gpio/export", "w");
	fprintf(fexport23, "23");
	fclose(fexport23);
	FILE *fexport24 = fopen("/sys/class/gpio/export", "w");
	fprintf(fexport24, "24");
	fclose(fexport24);

	lcd_put_number(0);

	char but18, but23, but24;
	char but_buf18 = '1';
	char but_buf23 = '1';
	char but_buf24 = '1';
	int calc = 0;
	int counter = 0;

	int but23_timerem;
	while(1){
		FILE *fbut18 = fopen("/sys/class/gpio/gpio18/value", "r");
		but18 = fgetc(fbut18);
		fclose(fbut18);
		FILE *fbut23 = fopen("/sys/class/gpio/gpio23/value", "r");
		but23 = fgetc(fbut23);
		fclose(fbut23);
		FILE *fbut24 = fopen("/sys/class/gpio/gpio24/value", "r");
		but24 = fgetc(fbut24);
		fclose(fbut24);

		if(but_buf18 != but18){
			but_buf18 = but18;
			if(but18 == '1'){//if button is released
				counter += 1;
				lcd_put_number(counter);
			}
		}
		if(but_buf23 != but23){
			but_buf23 = but23;
			if(but23 == '0'){//if button is pushed now
				but23_timerem = get_sec();
			}
			if(but23 == '1'){//if button is released
				int cur_sec = get_sec();
				int sec_dif = cur_sec - but23_timerem;
				if(sec_dif < 0){
					sec_dif += 60;
				}
				if(sec_dif <= 1){
					counter = 0;
					lcd_put_number(counter);
				} else if(sec_dif >=2){
					break;
				}
			}
		}
		if(but_buf24 != but24){
			but_buf24 = but24;
			if(but24 == '1'){//if button is released
				counter -= 1;
				lcd_put_number(counter);
			}
		}

		info_out();
	}

	FILE *funexport18 = fopen("/sys/class/gpio/unexport", "w");
	fprintf(funexport18, "18");
	fclose(funexport18);
	FILE *funexport23 = fopen("/sys/class/gpio/unexport", "w");
	fprintf(funexport23, "23");
	fclose(funexport23);
	FILE *funexport24 = fopen("/sys/class/gpio/unexport", "w");
	fprintf(funexport24, "24");
	fclose(funexport24);
}

int main(int argc, char* argv[]) {
	int ret = 0;

	gpio_init(GPIO_PIN_RESET);
	gpio_init(GPIO_PIN_DC);

	lcd_reset();

	fd = open(device, O_RDWR);
	if (fd < 0)
		printf("can't open device\n");

	ret = ioctl(fd, SPI_IOC_WR_MODE, &mode);
	if (ret == -1)
		printf("can't set spi mode\n");
	ret = ioctl(fd, SPI_IOC_RD_MODE, &mode);
	if (ret == -1)
		printf("can't get spi mode\n");

	ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
	if (ret == -1)
		printf("can't set bits per word\n");
	ret = ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &bits);
	if (ret == -1)
		printf("can't get bits per word\n");

	ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
	if (ret == -1)
		printf("can't set max speed hz\n");
	ret = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
	if (ret == -1)
		printf("can't get max speed hz\n");

	lcd_init_ili9341();
	printf("LCD init ok\n");
	lcd_set_address_window(0, 0, LCD_WIDTH - 1, LCD_HEIGHT - 1);

	lcd_fill_screen(COLOR_BLACK);

	button_calc();

	lcd_fill_screen(COLOR_CYAN);

	close(fd);

	gpio_free(GPIO_PIN_RESET);
	gpio_free(GPIO_PIN_DC);

	return EXIT_SUCCESS;
}


