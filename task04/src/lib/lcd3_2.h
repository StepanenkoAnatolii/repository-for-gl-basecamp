#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include <time.h>
#include <string.h>
#include "ili9341.h"
#include "fonts.h"

int gpio_init(unsigned int);
int gpio_free(unsigned int);
int gpio_set_value(unsigned int, unsigned int);
void lcd_reset(void);
void spi_write(uint8_t*, uint16_t);
void lcd_write_command(uint8_t);
void lcd_write_data(uint8_t*, unsigned long);
void lcd_init_ili9341(void);
void lcd_set_address_window(uint16_t, uint16_t, uint16_t, uint16_t);
void lcd_update_screen(void);
void lcd_draw_pixel(uint16_t, uint16_t, uint16_t);
void lcd_fill_rectangle(uint16_t, uint16_t, uint16_t, uint16_t, uint16_t);
void lcd_fill_screen(uint16_t);
void lcd_put_char(uint16_t, uint16_t, char, FontDef, uint16_t, uint16_t);
void lcd_put_string(uint16_t, uint16_t, char*, FontDef, uint16_t, uint16_t);
