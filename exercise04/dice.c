#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void) {
  char *name1 = (char *)malloc(20 * sizeof(char));
  printf("Player 1, input your name: ");
  scanf("%s", name1);

  char *name2 = (char *)malloc(20 * sizeof(char));
  printf("Player 2, input your name: ");
  scanf("%s", name2);

  srand(time(NULL));
  int score1 = rand() % 6 + 1;
  int score2 = rand() % 6 + 1;

  printf("%s has %d points\n%s has %d points\n", name1, score1, name2, score2);
  if (score1 > score2) {
    printf("%s wins!\n", name1);
  } else {
    printf("%s wins!\n", name2);
  };

  free(name1);
  free(name2);

  return 0;
}