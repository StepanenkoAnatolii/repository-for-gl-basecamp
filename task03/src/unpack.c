#include "unpack.h"

void unpack(char *infilename, char *outfilename){
	FILE *infile;
	infile = fopen(infilename, "r");
	if(infile == NULL){
		puts("Error: couldn't open source file");
		return;
	}
	char *buffer = malloc(sizeof(char) * get_file_size(infilename) * 2);//think about how not to mult by 2
	strcpy(buffer, "");
	char symbol;

	while((symbol = fgetc(infile)) != EOF){
		if(isdigit(symbol)){
			int counter = symbol - '0';
			symbol = fgetc(infile);
			for(;counter > 0; counter--){
				sprintf(buffer + strlen(buffer), "%c", symbol);
			}
		}
		else if(symbol == '&'){
			while(isdigit(symbol = fgetc(infile))){
				sprintf(buffer + strlen(buffer), "%c", symbol);
			}
			//ungetc(symbol, infile);
		}
		else{
			sprintf(buffer + strlen(buffer), "%c", symbol);
		}
	}

	FILE *outfile;
	outfile = fopen(outfilename, "w+");
	if(outfile == NULL){
		puts("Error: couldn't open file for output");
		return;
	}
	fprintf(outfile, "%s", buffer);

	free(buffer);
	fclose(infile);
	fclose(outfile);
}