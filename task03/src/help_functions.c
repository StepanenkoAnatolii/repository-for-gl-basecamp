#include "help_functions.h"

int get_file_size(char *filename){
	FILE *file = fopen(filename, "r");
	fseek(file, 0, SEEK_END);
	int file_size = ftell(file);
	fclose(file);
	return file_size;
}