#include "pack.h"

void pack(char *infilename, char *outfilename){
	FILE *infile;
	infile = fopen(infilename, "r");
	if(infile == NULL){
		puts("Error: couldn't open source file");
		return;
	}
	char *buffer = malloc(sizeof(char) * get_file_size(infilename) + 1);
	strcpy(buffer, "");
	char symbol;
	char buf_symbol = fgetc(infile);

	while((symbol = fgetc(infile)) != EOF){
		if(symbol == buf_symbol){
			int counter = 2;
			while((symbol = fgetc(infile)) == buf_symbol){
				counter++;
			}
			sprintf(buffer + strlen(buffer), "%d%c", counter, buf_symbol);
			buf_symbol = symbol;
		}
		else if(isdigit(symbol)){
			sprintf(buffer + strlen(buffer), "%c", buf_symbol);
			sprintf(buffer + strlen(buffer), "%c", '&');
			sprintf(buffer + strlen(buffer), "%c", symbol);

			while(isdigit(symbol = fgetc(infile))){
				sprintf(buffer + strlen(buffer), "%c", symbol);
			}
			sprintf(buffer + strlen(buffer), "%c", '&');
			
			buf_symbol = symbol;
		}
		else{
			sprintf(buffer + strlen(buffer), "%c", buf_symbol);
			buf_symbol = symbol;
		}
	}

	FILE *outfile;
	outfile = fopen(outfilename, "w+");
	if(outfile == NULL){
		puts("Error: couldn't open file for output");
		return;
	}
	fprintf(outfile, "%s", buffer);

	free(buffer);
	fclose(infile);
	fclose(outfile);
}