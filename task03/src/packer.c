#include "packer.h"

int main(int argc, char *argv[]) {
	char *infilename;
	char *outfilename;
	bool is_pack;
	bool is_chosen_pack = 0;
	bool is_chosen_in = 0;
	bool is_chosen_out = 0;

	for(int i = 1; i < argc; i++){
		char *key = argv[i];

		if(strcmp(key, "-pack") == 0){
			char *filename = argv[i+1];
			is_chosen_pack = 1;
			is_pack = 1;
			continue;
		}
		else if(strcmp(key, "-unpack") == 0){
			char *filename = argv[i+1];
			is_chosen_pack = 1;
			is_pack = 0;
			continue;
		}
		else if(strcmp(key, "-in") == 0){
			infilename = argv[i+1];
			is_chosen_in = 1;
			i++;
			continue;
		}
		else if(strcmp(key, "-out") == 0){
			outfilename = argv[i+1];
			is_chosen_out = 1;
			i++;
			continue;
		}
		else if(strcmp(key, "-help") == 0){
			printf("\n%s\n", help_message);
			return 0;
		}
		else{
			puts("Error: unknown key.\n");
			break;
		}
	}

	if(is_chosen_pack && is_chosen_in){
			if(!is_chosen_out){
				outfilename = infilename;
			}
			if(is_pack == 1){
				pack(infilename, outfilename);
			}
			else if(is_pack == 0){
				unpack(infilename, outfilename);
			}
		}
		else{
			puts("Error. You didn't choose what to do or files");
		}

	return 0;
}