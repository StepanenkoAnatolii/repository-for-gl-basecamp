#ifndef UNPACK_H
#define UNPACK_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "help_functions.h"

void unpack(char *infilename, char *outfilename);

#endif