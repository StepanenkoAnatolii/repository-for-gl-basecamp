#ifndef PACK_H
#define PACK_H

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "help_functions.h"

void pack(char *infilename, char *outfilename);

#endif