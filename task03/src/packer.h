#ifndef PACKER_H
#define PACKER_H

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <stdbool.h>
#include "pack.h"
#include "unpack.h"

static char *help_message = "This program packs or unpacks your file.\nUse keys:\n\"-pack\" to pack your file.\n\"-unpack\" to unpack your file.\n\"-in\" to choose input file by the next argument.\n\"-out\" to choose output file by the next argument.\n";

#endif