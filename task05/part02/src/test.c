#include "stdio.h"
#include "stdlib.h"
#include "string.h"

static const char* proc_curr_f = "/proc/curr_conv/currency";
static const char* proc_amount_f = "/proc/curr_conv/amount";
static const char* sys_curr_f = "/sys/class/conv_mod/method";

void add_currency(char* command);
void set_currency(char* command);
void set_amount(char* command);
void get_amount(char* result_str);

int main(void)
{
	int test_result = 1;
	char* result_amount_str = malloc(sizeof(char) * 20);

	//system("sudo bash");
	system("sudo insmod converter.ko");

	/*---------------main_part_begin---------------*/
	//----------test1----------//conversion to lower currency//
	add_currency("EUR31,440");
	add_currency("UAH1,0");

	set_amount("1074,738");

	set_currency("EUR-UAH");

	get_amount(result_amount_str);
	printf("1074,738 EUR = %s UAH; Must be 33789,76;\n", result_amount_str);
	if(strcmp(result_amount_str, "33789,76") != 0)
	{
		test_result = 0;
	}
	//-------------------------//
	//----------test2----------//conversion to higher currency//
	add_currency("USD26,710");
	add_currency("PLN6,800");

	set_amount("768,28");//it equals to 768,028

	set_currency("PLN-USD");

	get_amount(result_amount_str);
	printf("768,028 PLN = %s USD; Must be 195,79;\n", result_amount_str);
	if(strcmp(result_amount_str, "195,79") != 0)
	{
		test_result = 0;
	}
	//-------------------------//
	//----------test3----------//changin existing currency factor//
	add_currency("EUR30,547");

	set_amount("1074,738");
	set_currency("EUR-USD");

	get_amount(result_amount_str);
	printf("1074,738 EUR = %s USD; Must be 1228,425;\n", result_amount_str);
	if(strcmp(result_amount_str, "1228,425") != 0)
	{
		test_result = 0;
	}
	//-------------------------//
	//----------test4----------//working with big numbers//
	set_amount("2074210,293");
	set_currency("EUR-UAH");

	get_amount(result_amount_str);
	printf("2074210,293 EUR = %s UAH; Must be 63360901,82;\n", result_amount_str);
	if(strcmp(result_amount_str, "63360901,82") != 0)
	{
		test_result = 0;
	}
	//-------------------------//
	/*----------------main_part_end----------------*/

	system("sudo rmmod converter");

	free(result_amount_str);

	if(test_result == 1)//analyze test result
	{
		printf("Test passed\n");
	} else
	{
		printf("Test NOT passed\n");
	}
	
	return 0;
}

void add_currency(char* command)
{
	FILE *sys_currency = fopen(sys_curr_f, "w");
	fprintf(sys_currency, "%s", command);
	fclose(sys_currency);
}
void set_currency(char* command)
{
	FILE *proc_currency = fopen(proc_curr_f, "w");
	fprintf(proc_currency, "%s", command);
	fclose(proc_currency);
}
void set_amount(char* command)
{
	FILE *proc_amount = fopen(proc_amount_f, "w");
	fprintf(proc_amount, "%s", command);
	fclose(proc_amount);
}
void get_amount(char* result_str)
{
	FILE *proc_amount = fopen(proc_amount_f, "r");
	fscanf(proc_amount, "%s", result_str);
	fclose(proc_amount);
}