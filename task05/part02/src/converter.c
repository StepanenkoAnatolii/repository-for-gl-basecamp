#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/moduleparam.h>
#include <linux/stat.h>
#include <linux/slab.h>
#include <linux/ctype.h>
//for sysfs:
#include <linux/fs.h>
#include <linux/cdev.h>
#include <asm/uaccess.h>
#include <linux/pci.h>
//for procfs:
#include <linux/proc_fs.h>
#include <linux/sched.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Anatolii");
MODULE_DESCRIPTION("Module to modificate strings");

#define NUM_STR_SIZE 20
#define CURRENCY_BUFFER_S 1024

/*-------------------base_logic_start-------------------*/
static char* copy_str(char* str){
    char* new_str = kmalloc(strlen(str)+1, GFP_KERNEL);
    int i = 0;
    char ch;
    while((ch = str[i]) != '\0')
    {
        new_str[i] = ch;
        i++;
    }
    return new_str;
}

int pow_n(int val, int degree)
{
    int multiptier = val;
    if(degree == 0)
    {
        return 1;
    }

    while((degree-1) > 0)
    {
        val *= multiptier;
        degree--;
    }

    return val;
}

int round_to_power(int value, int power)
{
    int compare = 1;
    while(power > 0){
        compare *= 10;
        power--;
    }
    while(value >= compare)
    {
        value /= 10;
    }
    return value;
}

typedef struct float_val
{
    int primary;
    int secondary;//in range from 0 to 999
}float_val;

void float_to_str(float_val value, char* str)
{
    value.secondary = round_to_power(value.secondary, 3);
    if(value.secondary > 99)
    {
        snprintf(str, NUM_STR_SIZE, "%d,%d", value.primary, value.secondary);
    } else if(value.secondary > 9)
    {
        snprintf(str, NUM_STR_SIZE, "%d,0%d", value.primary, value.secondary);
    } else
    {
        snprintf(str, NUM_STR_SIZE, "%d,00%d", value.primary, value.secondary);
    }
    
}

float_val exchange(float_val quantity, float_val conv_factor)
{
    int primary;
    int secondary;
    float_val result;

    int primary_row = quantity.primary * conv_factor.primary;
    int primary_secondary1 = quantity.primary * conv_factor.secondary;
    int primary_secondary2 = quantity.secondary * conv_factor.primary;
    int secondary_row = quantity.secondary * conv_factor.secondary;
    secondary_row = secondary_row / 1000;

    primary = primary_row + primary_secondary1/1000 + primary_secondary2/1000;
    secondary = secondary_row + (primary_secondary1 % 1000) + (primary_secondary2 % 1000);
    if(secondary > 999)
    {
        int additional_p = 0;
        additional_p = secondary / 1000;
        primary += additional_p;
        secondary -= additional_p * 1000;
    }
    result.primary = primary;
    result.secondary = secondary;

    return result;
}

float_val calc_factor(float_val factor_from, float_val factor_to)
{
    int i;
    float_val result = {0,0,};
    int composit_factor = factor_to.primary*1000 + factor_to.secondary;
    int devidend_factor = factor_from.primary*1000 + factor_from.secondary;

    if(devidend_factor < composit_factor)
    {
        result.primary = 0;
    } else
    {
        result.primary = devidend_factor / composit_factor;
        devidend_factor = devidend_factor - ((devidend_factor / composit_factor) * composit_factor);
    }
    devidend_factor *= 10;

    i = 3;
    while(i > 0)
    {
        if(devidend_factor < composit_factor)
        {
            devidend_factor *=10;
            i--;
            continue;
        } else
        {
            result.secondary += pow_n(10, i-1) * (devidend_factor / composit_factor);
            devidend_factor = devidend_factor - ((devidend_factor / composit_factor) * composit_factor);
            devidend_factor *= 10;
        }
        i--;
    }
    return result;
}

float_val exchange_two_currs(float_val quantity, float_val conv_factor_from, float_val conv_factor_to)
{
    float_val res_factor = calc_factor(conv_factor_from, conv_factor_to);
    float_val result = exchange(quantity, res_factor);
    return result;
}


float_val str_to_float(char* str)
{
    long primary;
    long secondary;
    float_val result;
    int is_ok1;
    int is_ok2;
    char* primary_str = kmalloc(strlen(str), GFP_KERNEL);
    char* secondary_str = kmalloc(strlen(str), GFP_KERNEL);
    int counter = 0;
    int second_counter = 0;
    char ch;
    while((ch = str[counter]) != ',')
    {
        if(ch == '\0') 
        {
            float_val null_num = {0, 0,};
            kfree(primary_str);
            return null_num;
        }
        primary_str[counter] = ch;
        counter++;
    }
    counter++;

    while((ch = str[counter]) != '\0' && (ch != '\n'))
    {
        secondary_str[second_counter] = ch;
        counter++;
        second_counter++;
    }

    primary = 0;
    is_ok1 =  kstrtol(primary_str, 10, &primary);
    secondary = 0;
    is_ok2 = kstrtol(secondary_str, 10, &secondary);
    kfree(primary_str);
    kfree(secondary_str);

    if((is_ok1 != 0) || (is_ok2 != 0))
    {
        float_val res = {0,0,};
        return res;
    }

    result.primary = (int)primary;
    result.secondary = (int)secondary;

    return result;
}

//Currency map:
typedef struct curr_entry
{
    char* name;
    float_val factor;
}curr_entry;

typedef struct curr_map
{
    int size;
    curr_entry* curr_arr;
}curr_map;

float_val find_factor(char* name, curr_map c_map)
{
    int i = 0;
    float_val null_factor = {0,0,};

    while(i < c_map.size)
    {
        if(strcmp(c_map.curr_arr[i].name, name) == 0)
        {
            return c_map.curr_arr[i].factor;
        }
        i++;
    }

    return null_factor;
}

void curr_map_change_factor(curr_map* c_map, char* name, float_val factor)
{
    int i = 0;
    while(i < c_map->size)
    {
        if(strcmp(c_map->curr_arr[i].name, name) == 0)
        {
            c_map->curr_arr[i].factor = factor;
        }
        i++;
    }
}

void curr_map_add(curr_map* map, char* name, float_val factor)
{
    float_val old_factor = find_factor(name, *map);
    if(old_factor.primary == 0 && old_factor.secondary == 0)
    {
        int size = map->size;
        map->curr_arr[size].name = name;
        map->curr_arr[size].factor = factor;
        map->size += 1;
    } else{
        curr_map_change_factor(map, name, factor);
    }
    
}
/*--------------------base_logic_end--------------------*/
static float_val current_conv_factor = {1,0,};

static curr_entry* curr_arr;//currency array
static curr_map map1;

static int create_curr_map(void)
{
    curr_arr = kmalloc(CURRENCY_BUFFER_S, GFP_KERNEL);
    if(NULL == curr_arr)
    {
        return -ENOMEM;
    }
    map1.size = 0;
    map1.curr_arr = curr_arr;
    return 0;
}
static void cleanup_curr_map(void)
{
    if(curr_arr)
    {
        kfree(curr_arr);
        curr_arr = NULL;
    }
}
/*---------------sysfs-part-start----------------*/
#define LEN_MSG 20
static char buf_msg[ LEN_MSG + 1 ] = "0";

static ssize_t method_show( struct class *class, struct class_attribute *attr, char *buf ) {
   strcpy( buf, buf_msg );
   return strlen( buf );
}

static ssize_t method_store( struct class *class, struct class_attribute *attr, const char *buf, size_t count ) {
    char *word_curr;
    char *word_factor;
    int count1;
    int count2;
    char ch;
    float_val new_curr_amount;

   strncpy( buf_msg, buf, count );

   word_curr = kmalloc(sizeof(char)*4, GFP_KERNEL);
   count1 = 0;
   while(count1 < 3)
   {
    word_curr[count1] = buf[count1];
    count1++;
   }
   word_curr[4] = '\0';

   word_factor = kmalloc(sizeof(char)*20, GFP_KERNEL);
   count2 = 0;
   while(((ch = buf[count1]) != '\n') && (ch != '\0'))
   {
    word_factor[count2] = ch;
    count2++;
    count1++;
   }
   word_factor[count2] = '\0';

   new_curr_amount = str_to_float(word_factor);

   curr_map_add(&map1, word_curr, new_curr_amount);

   printk(KERN_INFO "sysfs:converter:new_currency:%s: %d,%d;\n", word_curr, new_curr_amount.primary, new_curr_amount.secondary);

   buf_msg[ count ] = '\0';
   return count;
}

CLASS_ATTR_RW(method);

static struct class *str_mod;
/*---------------sysfs-part-end------------------*/
/*---------------procfs-part-start---------------*/
#define MODULE_TAG      "currency_convertion_module "
#define PROC_DIRECTORY  "curr_conv"
#define PROC_AMOUNT_FILENAME   "amount"
#define PROC_CURRENCY_FILENAME  "currency"
#define CURR_BUFFER_SIZE     30
#define AMOUNT_BUFFER_SIZE  50

static char *proc_curr_buffer;
static char *proc_amount_buffer;
static size_t proc_msg_length;
static size_t proc_msg_read_pos;
static size_t proc_amount_msg_length;
static size_t proc_amount_msg_read_pos;

static struct proc_dir_entry *proc_dir;
static struct proc_dir_entry *proc_curr;
static struct proc_dir_entry *proc_amount;

/*--------------------amount_procfs_start--------------------*/
static ssize_t amount_read(struct file *file_p, char __user *buffer, size_t length, loff_t *offset);
static ssize_t amount_write(struct file *file_p, const char __user *buffer, size_t length, loff_t *offset);

static const struct proc_ops _proc_amount_ops = {
    .proc_read  = amount_read,
    .proc_write = amount_write,
};

static int create_amount_buffer(void)
{
    proc_amount_buffer = kmalloc(AMOUNT_BUFFER_SIZE, GFP_KERNEL);
    if (NULL == proc_amount_buffer)
        return -ENOMEM;

    proc_amount_msg_length = 0;

    return 0;
}

static void cleanup_amount_buffer(void)
{
    if (proc_amount_buffer) {
        kfree(proc_amount_buffer);
        proc_amount_buffer = NULL;
    }
    proc_amount_msg_length = 0;
}

static ssize_t amount_read(struct file *file_p, char __user *buffer, size_t length, loff_t *offset)
{//If call before amount_write, module gets killed
    size_t left;
    char *new_buffer;
    char *exchanged_str;
    float_val input;
    float_val exchanged;

    if (length > (proc_amount_msg_length - proc_amount_msg_read_pos))
        length = (proc_amount_msg_length - proc_amount_msg_read_pos);

    /*-----main-logic-start-----*/
    if(strcmp(proc_amount_buffer, "") == 0)
    {
        printk(KERN_INFO "There is no money yet\n");
        return 0;
    }

    input = str_to_float(proc_amount_buffer);
    exchanged = exchange(input, current_conv_factor);
    exchanged_str = kmalloc(strlen(proc_amount_buffer)*5, GFP_KERNEL);
    float_to_str(exchanged, exchanged_str);

    new_buffer = copy_str(exchanged_str);
    kfree(exchanged_str);
    /*------main-logic-end------*/

    left = copy_to_user(buffer, &new_buffer[proc_amount_msg_read_pos], length);

    kfree(new_buffer);

    proc_amount_msg_read_pos += length - left;

    if (left)
        printk(KERN_ERR MODULE_TAG "failed to read %lu from %lu chars\n", left, length);
    else
        printk(KERN_NOTICE MODULE_TAG "read %lu chars\n", length);

    return length - left;
}

static ssize_t amount_write(struct file *file_p, const char __user *buffer, size_t length, loff_t *offset)
{
    size_t msg_length;
    size_t left;

    if (length > AMOUNT_BUFFER_SIZE)
    {
        printk(KERN_WARNING MODULE_TAG "reduse message length from %lu to %u chars\n", length, AMOUNT_BUFFER_SIZE);
        msg_length = AMOUNT_BUFFER_SIZE;
    }
    else
        msg_length = length;

    left = copy_from_user(proc_amount_buffer, buffer, msg_length);

    proc_amount_msg_length = msg_length - left;
    proc_amount_msg_read_pos = 0;

    if (left)
        printk(KERN_ERR MODULE_TAG "failed to write %lu from %lu chars\n", left, msg_length);
    else
    {
        printk(KERN_NOTICE MODULE_TAG "written %lu chars\n", msg_length);
    }

    return length;
}
/*---------------------amount_procfs_end---------------------*/

/*--------------------curr_procfs_start--------------------*/
static ssize_t curr_read(struct file *file_p, char __user *buffer, size_t length, loff_t *offset);
static ssize_t curr_write(struct file *file_p, const char __user *buffer, size_t length, loff_t *offset);

static const struct proc_ops _proc_curr_ops = {
    .proc_read  = curr_read,
    .proc_write = curr_write,
};

static int create_curr_buffer(void)
{
    proc_curr_buffer = kmalloc(CURR_BUFFER_SIZE, GFP_KERNEL);
    if (NULL == proc_curr_buffer)
        return -ENOMEM;
    proc_msg_length = 0;

    return 0;
}

static void cleanup_curr_buffer(void)
{
    if (proc_curr_buffer) {
        kfree(proc_curr_buffer);
        proc_curr_buffer = NULL;
    }
    proc_msg_length = 0;
}

static ssize_t curr_read(struct file *file_p, char __user *buffer, size_t length, loff_t *offset)
{
    size_t left;
    char* new_buffer;

    if (length > (proc_msg_length - proc_msg_read_pos))
        length = (proc_msg_length - proc_msg_read_pos);

    /*-----main-logic-start-----*/
    new_buffer = copy_str(proc_curr_buffer);
    /*------main-logic-end------*/

    left = copy_to_user(buffer, &new_buffer[proc_msg_read_pos], length);

    kfree(new_buffer);

    proc_msg_read_pos += length - left;

    if (left)
        printk(KERN_ERR MODULE_TAG "failed to read %lu from %lu chars\n", left, length);
    else
        printk(KERN_NOTICE MODULE_TAG "read %lu chars\n", length);

    return length - left;
}

static ssize_t curr_write(struct file *file_p, const char __user *buffer, size_t length, loff_t *offset)
{
    size_t msg_length;
    size_t left;
    char* curr_buf1;
    char* curr_buf2;
    int count1;
    int count2;
    float_val factor1;
    float_val factor2;

    if (length > CURR_BUFFER_SIZE)
    {
        printk(KERN_WARNING MODULE_TAG "reduse message length from %lu to %u chars\n", length, CURR_BUFFER_SIZE);
        msg_length = CURR_BUFFER_SIZE;
    }
    else
        msg_length = length;

    left = copy_from_user(proc_curr_buffer, buffer, msg_length);

    /*-----main-logic-start-----*/
    curr_buf1 = kmalloc(sizeof(char)*4, GFP_KERNEL);
    count1 = 0;
    while(count1 < 3)
    {
        curr_buf1[count1] = proc_curr_buffer[count1];
        count1++;
    }
    curr_buf1[3] = '\0';
    count1++;

    curr_buf2 = kmalloc(sizeof(char)*4, GFP_KERNEL);
    count2 = 0;
    while(count2 < 3)
    {
        curr_buf2[count2] = proc_curr_buffer[count1];
        count2++;
        count1++;
    }
    curr_buf2[3] = '\0';

    factor1 = find_factor(curr_buf1, map1);
    factor2 = find_factor(curr_buf2, map1);

    if( ((factor1.primary != 0) || (factor1.secondary != 0)) && ((factor2.primary != 0) || (factor2.secondary != 0)) )
    {//if factor1 and factor2 are not null-factors
        current_conv_factor = calc_factor(factor1, factor2);
    }

    kfree(curr_buf1);
    kfree(curr_buf2);
    /*------main-logic-end------*/

    proc_msg_length = msg_length - left;
    proc_msg_read_pos = 0;

    if (left)
        printk(KERN_ERR MODULE_TAG "failed to write %lu from %lu chars\n", left, msg_length);
    else
    {
        printk(KERN_NOTICE MODULE_TAG "written %lu chars\n", msg_length);
    }

    return length;
}
/*---------------------curr_procfs_end---------------------*/

/*--------------------general_start--------------------*/
static int create_proc_fs(void)
{
    proc_dir = proc_mkdir(PROC_DIRECTORY, NULL);
    if (NULL == proc_dir)
        return -EFAULT;

    proc_amount = proc_create(PROC_AMOUNT_FILENAME, S_IFREG | S_IRUGO | S_IWUGO, proc_dir, &_proc_amount_ops);
    if (NULL == proc_amount)
        return -EFAULT;

    proc_curr = proc_create(PROC_CURRENCY_FILENAME, S_IFREG | S_IRUGO | S_IWUGO, proc_dir, &_proc_curr_ops);
    if (NULL == proc_curr)
        return -EFAULT;

    return 0;
}


static void cleanup_proc_fs(void)
{
    if (proc_amount)
    {
        remove_proc_entry(PROC_AMOUNT_FILENAME, proc_dir);
        proc_amount = NULL;
    }

    if (proc_curr)
    {
        remove_proc_entry(PROC_CURRENCY_FILENAME, proc_dir);
        proc_curr = NULL;
    }

    if (proc_dir)
    {
        remove_proc_entry(PROC_DIRECTORY, NULL);
        proc_dir = NULL;
    }
}
/*---------------------general_end---------------------*/

/*---------------procfs-part-end-----------------*/

static int __init str_mod_init(void)
{
    int err;
    int res;
    float_val uah_factor = {1,0,};
    printk(KERN_INFO "Converter module is loaded");
    /*---------------procfs-part-start---------------*/
    err = create_curr_buffer();
    if (err)
        goto error;

    err = create_amount_buffer();
    if(err)
        goto error;

    err = create_proc_fs();
    if (err)
        goto error;

    /*---------------procfs-part-end-----------------*/
    err = create_curr_map();
    if(err)
        goto error;

    curr_map_add(&map1, "UAH", uah_factor);
    /*---------------sysfs-part-start----------------*/
    str_mod = class_create( THIS_MODULE, "conv_mod" );
    if( IS_ERR( str_mod ) ) printk( "bad class create\n" );
    res = class_create_file( str_mod, &class_attr_method );
    /*---------------sysfs-part-end------------------*/
    return 0;

    error:
    printk(KERN_ERR MODULE_TAG "failed to load\n");
    cleanup_proc_fs();
    cleanup_curr_buffer();
    cleanup_amount_buffer();
    cleanup_curr_map();

    return err;
}

static void __exit str_mod_exit(void)
{
    printk(KERN_INFO "Converter module is unloaded");
    /*---------------procfs-part-start---------------*/
    cleanup_proc_fs();
    cleanup_curr_buffer();
    cleanup_amount_buffer();
    cleanup_curr_map();
    /*---------------procfs-part-end-----------------*/
    /*---------------sysfs-part-start----------------*/
    class_remove_file( str_mod, &class_attr_method );
    class_destroy( str_mod );
    /*---------------sysfs-part-end------------------*/
}

module_init(str_mod_init);
module_exit(str_mod_exit);