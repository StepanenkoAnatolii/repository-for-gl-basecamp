#include <linux/init.h>
#include <linux/module.h>
#include <linux/spi/spi.h>
#include <linux/delay.h>
#include <linux/gpio.h>
#include <linux/device.h>
#include "lcd_functions.h"

#include <linux/cdev.h>
#include <linux/fs.h>
#include <asm/uaccess.h>

#include <linux/kernel.h>
#include <linux/string.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Anatolii Stepanenko");
MODULE_DESCRIPTION("LCD module to work with barley-break table");

static int major = 0;
module_param(major, int, S_IRUGO);

#define EOK 0

int *str_to_table(char *str)//remember to kfree value that function returns
{
	int *arr = kmalloc(sizeof(int) * 9, GFP_KERNEL);
	char ch;
	int i = 0;

	while ((ch = str[i]) != '\0' && i < 9) {
		arr[i] = ch - '0';
		i++;
	}

	return arr;
}

static ssize_t dev_lcd_write(struct file *file, const char __user *buf, size_t count, loff_t *ppos)
{
	size_t maxdatalen = 12, ncopied;
	uint8_t databuf[12];//equal to maxdatalen
	int *table;
	char *str1;
	char *str2;
	char *str3;

	if (count < maxdatalen) {
		maxdatalen = count;
	}

	ncopied = copy_from_user(databuf, buf, maxdatalen);

	if (ncopied != 0) {
		printk(KERN_INFO "Error: could't copy %zd bytes from the user\n", ncopied);//log
	}

	databuf[maxdatalen] = 0;

	printk(KERN_INFO "LCD module receives: %s\n", databuf);//log

	if (strcmp(databuf, "999999999") == 0) {
		lcd_fill_screen(COLOR_BLACK);

		lcd_put_str(20, 50, "You Win!", Font_16x26, COLOR_WHITE, COLOR_BLACK);

		lcd_update_screen();
		return count;
	}
	if (strcmp(databuf, "000000000") == 0) {
		lcd_fill_screen(COLOR_BLACK);

		lcd_put_str(10, 50, "Game end!", Font_16x26, COLOR_WHITE, COLOR_BLACK);

		lcd_update_screen();
		return count;
	}

	table = str_to_table(databuf);

	str1 = kmalloc(sizeof(char) * 7, GFP_KERNEL);
	snprintf(str1, 6, "%d %d %d", table[0], table[1], table[2]);
	str1[6] = '\0';
	str2 = kmalloc(sizeof(char) * 7, GFP_KERNEL);
	snprintf(str2, 6, "%d %d %d", table[3], table[4], table[5]);
	str2[6] = '\0';
	str3 = kmalloc(sizeof(char) * 7, GFP_KERNEL);
	snprintf(str3, 6, "%d %d %d", table[6], table[7], table[8]);
	str3[6] = '\0';


	lcd_put_str(40, 19, str1, Font_16x26, COLOR_WHITE, COLOR_BLACK);
	lcd_put_str(40, 49, str2, Font_16x26, COLOR_WHITE, COLOR_BLACK);
	lcd_put_str(40, 79, str3, Font_16x26, COLOR_WHITE, COLOR_BLACK);
	lcd_update_screen();

	kfree(str1);
	kfree(str2);
	kfree(str3);

	kfree(table);

	return count;
}

static const struct file_operations dev_fops = {
	.owner = THIS_MODULE,
	.write = dev_lcd_write,
};

#define DEVICE_FIRST  0
#define DEVICE_COUNT  1
#define MODNAME "lcd_mod"

static struct cdev hcdev;

static void __exit mod_exit(void)
{
	//dev_start==========//
	cdev_del(&hcdev);
	unregister_chrdev_region(MKDEV(major, DEVICE_FIRST), DEVICE_COUNT);
	//dev_end============//

	gpio_free(LCD_PIN_DC);

	if (lcd_spi_device) {
		spi_unregister_device(lcd_spi_device);
	}
	pr_info("LCD: spi device unregistered\n");
	pr_info("LCD: module exited\n");
}

static int __init mod_init(void)
{
	int ret;
	struct spi_board_info lcd_info = {
		.modalias = "LCD",
		.max_speed_hz = 25e6,
		.bus_num = 0,
		.chip_select = 0,
		.mode = SPI_MODE_0,
	};
	struct spi_master *master;
/*--------------------dev_start--------------------*/
	dev_t dev;
	if (major != 0) {
		dev = MKDEV(major, DEVICE_FIRST);
		ret = register_chrdev_region(dev, DEVICE_COUNT, MODNAME);
	} else {
		ret = alloc_chrdev_region(&dev, DEVICE_FIRST, DEVICE_COUNT, MODNAME);
		major = MAJOR(dev);// не забыть зафиксировать!
	}

	if (ret < 0) {//Error handling
		printk(KERN_ERR "=== Can not register char device region\n");
		goto out;
	}

	cdev_init(&hcdev, &dev_fops);
	hcdev.owner = THIS_MODULE;
	ret = cdev_add(&hcdev, dev, DEVICE_COUNT);

	if (ret < 0) {//Error handling
		unregister_chrdev_region(MKDEV(major, DEVICE_FIRST), DEVICE_COUNT);
		printk(KERN_ERR "=== Can not add char device\n");
		goto out;
	}

	printk(KERN_INFO "=========== dev installed %d:%d ==============\n",
			MAJOR(dev), MINOR(dev));
/*---------------------dev_end---------------------*/

	master = spi_busnum_to_master(lcd_info.bus_num);
	if (!master) {
		printk(KERN_INFO "MASTER not found.\n");
		ret = -ENODEV;
		goto out;
	}

	lcd_spi_device = spi_new_device(master, &lcd_info);
	if (!lcd_spi_device) {
		printk(KERN_INFO "FAILED to create slave.\n");
		ret = -ENODEV;
		goto out;
	}

	lcd_spi_device->bits_per_word = 8,

	ret = spi_setup(lcd_spi_device);
	if (ret) {
		printk(KERN_INFO "FAILED to setup slave.\n");
		spi_unregister_device(lcd_spi_device);
		ret = -ENODEV;
		goto out;
	}

	pr_info("LCD: spi device setup completed\n");

	gpio_request(LCD_PIN_RESET, "LCD_PIN_RESET");
	gpio_direction_output(LCD_PIN_RESET, 0);
	gpio_request(LCD_PIN_DC, "LCD_PIN_DC");
	gpio_direction_output(LCD_PIN_DC, 0);
	lcd_reset();

	lcd_init_st7735();

	memset(frame_buffer, COLOR_BLACK, sizeof(frame_buffer));
	lcd_set_address_window(0, 0, LCD_WIDTH - 1, LCD_HEIGHT - 1);


	pr_info("LCD: module loaded\n");

	return 0;

out:
	return ret;
}

module_init(mod_init);
module_exit(mod_exit);
