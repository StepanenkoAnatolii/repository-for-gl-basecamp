#include <linux/module.h>
#include <linux/init.h>
#include <linux/gpio.h>
#include <linux/slab.h>

char *read_keyboard(int *vertical_pins, int *horizontal_pins);
char choose_char(int vertical, int horizontal);
void gpio_write(int gpio_num, int output);
int gpio_read(int gpio_num);

char *read_keyboard(int *vertical_pins, int *horizontal_pins)
{//remember to free result after function usage
	char *result = kmalloc(sizeof(char) * 17, GFP_KERNEL);
	int i;
	int j;
	int general_counter = 0;

	for (i = 0; i < 4; i++) {
		gpio_write(vertical_pins[i], 1);
		for (j = 0; j < 4; j++) {
			int but = gpio_read(horizontal_pins[j]);
			char present_char;
			if (but == 0)
				continue;
			present_char = choose_char(i, j);
			result[general_counter] = present_char;
			general_counter++;
		}
		gpio_write(vertical_pins[i], 0);
	}

	result[general_counter] = '\0';

	return result;
}

char choose_char(int vertical, int horizontal)
{
	char res = 'N';

	if (vertical == 0 && horizontal == 0)
		res = '1';
	if (vertical == 0 && horizontal == 1)
		res = '4';
	if (vertical == 0 && horizontal == 2)
		res = '7';
	if (vertical == 0 && horizontal == 3)
		res = '*';

	if (vertical == 1 && horizontal == 0)
		res = '2';
	if (vertical == 1 && horizontal == 1)
		res = '5';
	if (vertical == 1 && horizontal == 2)
		res = '8';
	if (vertical == 1 && horizontal == 3)
		res = '0';

	if (vertical == 2 && horizontal == 0)
		res = '3';
	if (vertical == 2 && horizontal == 1)
		res = '6';
	if (vertical == 2 && horizontal == 2)
		res = '9';
	if (vertical == 2 && horizontal == 3)
		res = '#';

	if (vertical == 3 && horizontal == 0)
		res = 'A';
	if (vertical == 3 && horizontal == 1)
		res = 'B';
	if (vertical == 3 && horizontal == 2)
		res = 'C';
	if (vertical == 3 && horizontal == 3)
		res = 'D';

	return res;
}

void gpio_write(int gpio_num, int output)
{
	int i = 0;

	char pin_name[10];
	sprintf(pin_name, "%s%d", "PIN", gpio_num);

	gpio_free(gpio_num);
	i = gpio_request(gpio_num, pin_name);
	if (i < 0)
		return;

	gpio_direction_output(gpio_num, output);

	gpio_free(gpio_num);
}

int gpio_read(int gpio_num)
{
	int i = 0;
	int res = -1;

	char pin_name[10];
	sprintf(pin_name, "%s%d", "PIN", gpio_num);

	gpio_free(gpio_num);
	i = gpio_request(gpio_num, pin_name);
	if (i < 0)
		return i;

	res = gpio_get_value(gpio_num);

	gpio_free(gpio_num);

	return res;
}