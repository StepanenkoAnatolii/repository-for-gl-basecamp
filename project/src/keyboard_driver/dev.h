#include "keyb_krn.h"//file with keyboard functions
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/module.h>
#include <asm/uaccess.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Anatolii Stepanenko");

#define BUF_LENGTH 17
static char result_buf[BUF_LENGTH];

static ssize_t dev_read(struct file *file, char *buf, size_t count, loff_t *ppos)
{
	char ch;
	int i;
	char *keyb_val;
	int len;

	int *verticalp = kmalloc(5 * sizeof(int), GFP_KERNEL);
	int *horizontalp = kmalloc(5 * sizeof(int), GFP_KERNEL);

	verticalp[0] = 21;
	verticalp[1] = 20;
	verticalp[2] = 16;
	verticalp[3] = 12;
	horizontalp[0] = 26;
	horizontalp[1] = 19;
	horizontalp[2] = 13;
	horizontalp[3] = 9;

	keyb_val = read_keyboard(verticalp, horizontalp);

	i = 0;
	while ((ch = keyb_val[i]) != '\0') {//copy keyb_val to buffer
		result_buf[i] = ch;
		i++;
	}
	result_buf[i] = '\0';
//----------------------------------------------------------------------//
	len = BUF_LENGTH;
	if (count < len)
		return -EINVAL;
	if (*ppos != 0) {
		return 0;
	}
	if (copy_to_user(buf, result_buf, len))
		return -EINVAL;
	*ppos = len;
	return len;
}

static int __init dev_init(void);
module_init(dev_init);

static void __exit dev_exit(void);
module_exit(dev_exit);
