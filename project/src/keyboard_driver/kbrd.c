#include "dev.h"
#include <linux/cdev.h>
#include <linux/init.h>

static int major = 0;
module_param(major, int, S_IRUGO);

#define EOK 0

static const struct file_operations dev_fops = {
	.owner = THIS_MODULE,
	.read  = dev_read,
};

#define DEVICE_FIRST  0
#define DEVICE_COUNT  1
#define MODNAME "kbrd_mod"

static struct cdev hcdev;

static int __init dev_init(void)
{
	int ret;
	dev_t dev;
	if (major != 0) {
		dev = MKDEV(major, DEVICE_FIRST);
		ret = register_chrdev_region(dev, DEVICE_COUNT, MODNAME);
	} else {
		ret = alloc_chrdev_region(&dev, DEVICE_FIRST, DEVICE_COUNT, MODNAME);
		major = MAJOR(dev);
	}

	if (ret < 0) {//Error handling
		printk(KERN_ERR "=== Can not register char device region\n");
		goto err;
	}

	cdev_init(&hcdev, &dev_fops);
	hcdev.owner = THIS_MODULE;
	ret = cdev_add(&hcdev, dev, DEVICE_COUNT);

	if (ret < 0) {//Error handling
		unregister_chrdev_region(MKDEV(major, DEVICE_FIRST), DEVICE_COUNT);
		printk(KERN_ERR "=== Can not add char device\n");
		goto err;
	}

	printk(KERN_INFO "=========== module installed %d:%d ==============\n",
			MAJOR(dev), MINOR(dev));
err:
	return ret;
}

static void __exit dev_exit(void)
{
	cdev_del(&hcdev);
	unregister_chrdev_region(MKDEV(major, DEVICE_FIRST), DEVICE_COUNT);
	printk(KERN_INFO "=============== module removed ==================\n");
}
