#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "time.h"

#define TABLE_SIZE 3
#define DIFFICULTY 10

#define UP_SIGN '2'
#define DOWN_SIGN '8'
#define LEFT_SIGN '4'
#define RIGHT_SIGN '6'
#define END_SIGN 'D'
#define NEW_GAME_SIGN 'C'

static char *help_message = "To move 0 in directions you need, input:\n'2' - up\n'8' - down\n'4' - left\n'6' - right\n'C' - new game\n'D' - end game\n";

typedef struct coordinates{
	int col;
	int row;
} coordinates;

int main_cycle(void);
void print_table(int **arr);
void print_str_table(char *str);
void initialize_table(int **arr);
int is_table_finished(int **arr);
int move_zero(int **arr, char direction);
void mix_table(int **arr, int quant);//quant (quantity) is the quantity on moves to mix; the more quant, the harder
coordinates find_zero(int **arr);
void swap(int *v1, int *v2);
char get_char(void);


int main(void)
{
	int result = main_cycle();

	return result;
}

int main_cycle(void)
{
	int i = 0;
	int j = 0;

	//allocating table array:
	int **arr = malloc(sizeof(int *) * TABLE_SIZE);
	for (i = 0; i < TABLE_SIZE; i++) {
		arr[i] = malloc(sizeof(int) * TABLE_SIZE);
	}


	printf("%s", help_message);

	initialize_table(arr);

	mix_table(arr, DIFFICULTY);
	print_table(arr);

	while (is_table_finished(arr) != 0) {
		char ch;
		ch = get_char();
		int check = move_zero(arr, ch);
		if (check == 0) {
			print_table(arr);
		} else {
			if (ch == END_SIGN) {
				print_str_table("000000000");
				return 0;
			}
			if (ch == NEW_GAME_SIGN) {
				usleep(150000);
				main_cycle();
				return 0;
			}
		}
		usleep(150000);
	}
	print_str_table("999999999");


	//releasing memory:
	for (i = 0; i < TABLE_SIZE; i++) {
		free(arr[i]);
	}
	free(arr);
	return 0;
}

char get_char(void)
{
	char res;

	FILE *f_kbrd = fopen("../keyboard_driver/kbrd", "r");
	res = fgetc(f_kbrd);
	fclose(f_kbrd);

	return res;
}

int is_table_finished(int **arr)//0 if finished; 1 if not
{
	int res = 0;
	int counter = 0;
	int i = 0;
	int j = 0;
	for (i = 0; i < TABLE_SIZE; i++) {
		for (j = 0; j < TABLE_SIZE; j++) {
			if ((counter+1 != arr[i][j]) && counter+1 != 9) {
				res = 1;
			}
			counter++;
		}
	}

	return res;
}

void mix_table(int **arr, int quant)
{
	int i = 0;
	srand(time(NULL));
	rand();

	for (i = 0; i < quant; i++) {
		char direction = 'x';
		int rand_v = rand() % 4;
		if (rand_v == 0)
			direction = UP_SIGN;
		if (rand_v == 1)
			direction = DOWN_SIGN;
		if (rand_v == 2)
			direction = LEFT_SIGN;
		if (rand_v == 3)
			direction = RIGHT_SIGN;

		int is_ok = move_zero(arr, direction);
		if (is_ok != 0) {
			i--;
			continue;
		}
	}
}

int move_zero(int **arr, char direction)
{
	coordinates zero_coord = find_zero(arr);
	if (direction == UP_SIGN) {//move up ('u')
		if (zero_coord.col-1 < 0) {
			return -1;//there is no upper element
		}
		swap(&(arr[zero_coord.col][zero_coord.row]), &(arr[zero_coord.col-1][zero_coord.row]));
		return 0;
	}
	if (direction == DOWN_SIGN) {//move down ('d')
		if (zero_coord.col+1 > (TABLE_SIZE-1)) {
			return -2;//there is no lower element
		}
		swap(&(arr[zero_coord.col][zero_coord.row]), &(arr[zero_coord.col+1][zero_coord.row]));
		return 0;
	}
	if (direction == LEFT_SIGN) {//move left ('l')
		if (zero_coord.row-1 < 0) {
			return -3;//there is no more to the left element
		}
		swap(&(arr[zero_coord.col][zero_coord.row]), &(arr[zero_coord.col][zero_coord.row-1]));
		return 0;
	}
	if (direction == RIGHT_SIGN) {//move right ('r')
		if (zero_coord.row+1 > (TABLE_SIZE-1)) {
			return -4;//there is no more to the right element
		}
		swap(&(arr[zero_coord.col][zero_coord.row]), &(arr[zero_coord.col][zero_coord.row+1]));
		return 0;
	}

	return -5;//incorrect direction
}

coordinates find_zero(int **arr)
{
	int i = 0;
	int j = 0;
	for (i = 0; i < TABLE_SIZE; i++) {
		for (j = 0; j < TABLE_SIZE; j++) {
			if (arr[i][j] == 0) {
				coordinates zero_coord = {i, j,};
				return zero_coord;
			}
		}
	}
	coordinates null_coord = {-1, -1,};
	return null_coord;//if zero not found
}

void swap(int *v1, int *v2)
{
	*v1 = *v1 + *v2;
	*v2 = *v1 - *v2;
	*v1 = *v1 - *v2;
}

void initialize_table(int **arr)
{
	int i = 0;
	int j = 0;
	int counter = 1;

	for (i = 0; i < TABLE_SIZE; i++) {
		for (j = 0; j < TABLE_SIZE; j++) {
			arr[i][j] = counter;
			counter++;
		}
	}

	arr[TABLE_SIZE-1][TABLE_SIZE-1] = 0;
}

void print_str_table(char *str)
{
	FILE *f_lcd = fopen("../lcd_driver/lcd_b", "w");
	fprintf(f_lcd, str);
	fclose(f_lcd);
}

void print_table(int **arr)
{
	int i = 0;
	int j = 0;

	char *table_str = malloc(sizeof(char) * 10);
	table_str[0] = '\0';
	for (i = 0; i < TABLE_SIZE; i++) {
		for (j = 0; j < TABLE_SIZE; j++) {
			sprintf(table_str, "%s%d", table_str, arr[i][j]);
		}
	}
	table_str[9] = '\0';

	print_str_table(table_str);
}
